# README

## Usage

- Make sure you have ansible installed
- Clone this repo
- Create a `hosts` file (copy from `hosts.sample`)
- Set `install_dir` in your hosts file to the folder where you'd like the installation to be
- Run `ansible-playbook -i hosts devhost.yaml`

Mailman core and the Hyperkitty plugin are installed in the `py34` virtualenv, mailman.client, Postorius and Hyperkitty are living in the `py27` one.
